<?php

namespace _Project_\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Container\ContainerInterface;
use Atlas\Mapper\Record as AtlasRecord;

abstract class Bread extends Controller
{

    protected $mapper_class;
    protected $route_names = [
        'browse'=>'browse',
        'read'=>'read',
        'add'=>'add',
        'edit'=>'edit',
        'delete'=>'delete',
    ];
    protected $form_template = 'common/form';
    protected $delete_template = 'common/delete';
    protected $browse_template = 'common/browse';
    protected $read_template = 'common/read';
    protected $order_by;
    protected $limit = 25;

    const ACTION_EDIT = 'EDIT';
    const ACTION_ADD = 'ADD';

    public function __construct(ContainerInterface $container, ServerRequestInterface $request, ResponseInterface $response)
    {
        parent::__construct($container, $request, $response);

        $this->setDefaultRouteNames();
    }

    /**
     * Get a list of all the records
     * 
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function browse()
    {
        $resource_name = strtolower($this->getClassShortName($this->mapper_class));

        if (! $this->acl->isAllowed($this->user, $resource_name, 'browse')) {
            return $this->notAllowed();
        }

        $page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);

        if (! $page) {
            $page = 1;
        }
        
        
        $select = $this->atlas->select($this->mapper_class);
        if ($this->order_by) {
            $select->orderBy($this->orderBy);
        }

        $select->page($page)->perPage($this->limit);

        $select = $this->modifySelect($select);

        $records = $select->fetchRecordSet();
        $total = $select->fetchCount();
        $this->paginator->setPaging($this->limit);
        $pagination = $this->paginator->paginate($page, $total, $this->request->getUri());

        $table_class = $this->mapper_class . 'Table';

        $content = $this->view->render($this->browse_template, [
            'messages'=>$this->messages,
            'records'=>$records,
            'columns'=>$table_class::COLUMN_NAMES,
            'pagination'=>$pagination,
            'read_route_name'=>$this->route_names['read'],
            'add_route_name'=>$this->route_names['add'],
            'record_type'=>$this->getClassShortName($this->mapper_class)
        ]);
        $this->response->getBody()->write($content);
        return $this->response;
    }

    public function read($id)
    {
        $record = $this->atlas->fetchRecord($this->mapper_class, $id);

        if (! $record) {
            return $this->notFound();
        }
        
        if (! $this->acl->isAllowed($this->user, $record, 'read')) {
            return $this->notAllowed();
        }

        $table_class = $this->mapper_class . 'Table';

        $content = $this->view->render($this->read_template, [
            'messages'=>$this->messages,
            'record'=>$record,
            'record_type'=>$this->getClassShortName($this->mapper_class),
            'columns'=>$table_class::COLUMN_NAMES,
            'edit_route_name'=>$this->route_names['edit'],
            'delete_route_name'=>$this->route_names['delete'],
            'browse_route_name'=>$this->route_names['browse'],
        ]);
        $this->response->getBody()->write($content);
        return $this->response;
    }

    /**
     * Show the add new record form
     * 
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function add()
    {
        $record = $this->atlas->newRecord($this->mapper_class);

        if (! $this->acl->isAllowed($this->user, $record, 'add')) {
            return $this->notAllowed();
        }

        $content = $this->buildForm($record, self::ACTION_ADD);

        $this->response->getBody()->write($content);
        return $this->response;
    }

    /**
     * Perform the add new record
     * 
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function doAdd()
    {
        $record = $this->atlas->newRecord($this->mapper_class);

        if (! $this->acl->isAllowed($this->user, $record, 'add')) {
            return $this->notAllowed();
        }

        $data = $this->sanitizePost();
        

        $record->set($data);

        $record->setCreatedUpdated(self::ACTION_ADD, $this->user);

        try {
            $this->atlas->insert($record);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->trigger('insert-error', $record, $message);
            $content = $this->buildForm($record, self::ACTION_ADD);
            $this->response->getBody()->write($content);
            return $this->response;
        };

        $this->trigger('insert', $record);
        return $this->redirect($this->router->urlFor($this->route_names['read'], ['id'=>$record->getPrimaryVal()]));
    }

    /**
     * Show the edit record form
     * 
     * @param mixed $id The primary key value
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function edit($id)
    {
        $record = $this->atlas->fetchRecord($this->mapper_class, $id);

        if (! $record) {
            return $this->notFound();
        }

        if (! $this->acl->isAllowed($this->user, $record, 'edit')) {
            return $this->notAllowed();
        }

        $content = $this->buildForm($record, self::ACTION_EDIT);

        $this->response->getBody()->write($content);
        return $this->response;
    }

    /**
     * Perform the edit
     * 
     * @param mixed $id The primary key value
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function doEdit($id)
    {
        $record = $this->atlas->fetchRecord($this->mapper_class, $id);

        if (! $record) {
            return $this->notFound();
        }

        if (! $this->acl->isAllowed($this->user, $record, 'edit')) {
            return $this->notAllowed();
        }

        $data = $this->sanitizePost();

        $record->set($data);

        $record->setCreatedUpdated(self::ACTION_EDIT, $this->user);
        

        try {
            $this->atlas->update($record);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->trigger('update-error', $record, $message);
            $content = $this->buildForm($record, self::ACTION_EDIT);
            $this->response->getBody()->write($content);
            return $this->response;
        };

       

        $this->trigger('update', $record);
        return $this->redirect($this->router->urlFor($this->route_names['read'], ['id'=>$record->getPrimaryVal()]));
    }

    /**
     * Show the delete record form
     * 
     * @param mixed $id The primary key value
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete($id)
    {
        
        $mapper = $this->atlas->mapper($this->mapper_class);
        $relationships = $mapper->getRelationships();
        $with = [];
        foreach (array_keys($relationships->getFields()) as $field) {
            if (get_class($relationships->get($field)) == 'Atlas\Mapper\Relationship\OneToMany') {
                $with[] = $field;
            }
        }
        
        
        $record = $this->atlas->fetchRecord($this->mapper_class, $id, $with);

        if (! $record) {
            return $this->notFound();
        }

        if (! $this->acl->isAllowed($this->user, $record, 'delete')) {
            return $this->notAllowed();
        }

        $related = [];
        foreach ($with as $rel_name) {
            if (count($record->$rel_name) > 0) {
                $related[$rel_name] = count($record->$rel_name);
            }
        }
        

        $csrfToken = $this->form->hidden()->idName('csrf_token')->value($this->session->getCsrfToken()->getValue());
        $this->form->addElement('csrf_token', $csrfToken);

        $cancel_href = $this->router->urlFor($this->route_names['read'], ['id'=>$record->getPrimaryVal()]);

        $content = $this->view->render($this->delete_template, [
            'messages'=>$this->messages,
            'form'=>$this->form,
            'record'=>$record,
            'record_type'=>$this->getClassShortName($this->mapper_class),
            'cancel_href'=>$cancel_href,
            'related'=>$related
        ]);

        $this->response->getBody()->write($content);
        return $this->response;
    }

    /**
     * Perform the delete
     * 
     * @param mixed $id The primary key value
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function doDelete($id)
    {
        $record = $this->atlas->fetchRecord($this->mapper_class, $id);

        if (! $record) {
            return $this->notFound();
        }

        if (! $this->acl->isAllowed($this->user, $record, 'delete')) {
            return $this->notAllowed();
        }
        
        // This does not directly or explicitly delete relateds!

        try {
            $this->atlas->delete($record);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->trigger('delete-error', $record, $message);
            $href = $this->router->urlFor($this->route_names['delete'], [$record->getPrimaryCol()=>$record->getPrimaryVal()]);
            return $this->redirect($href);
        }

        $this->trigger('delete', $record);
        $href = $this->router->urlFor($this->route_names['browse']);
        return $this->redirect($href);
    }

    /**
     * Build the form based on the record
     * 
     * @param AtlasRecord $record
     * @param string $action
     */
    protected function buildForm(AtlasRecord $record, $action)
    {
        $table_name = $this->getTableName($this->mapper_class);
        $data = $record->getArrayCopy();

        $this->beforeBuildForm();

        $this->form->build($table_name, $data);
        $csrf = $this->form->hidden()->idName('csrf_token')->value($this->session->getCsrfToken()->getValue());
        $this->form->addElement('csrf', $csrf);

        $labels = [];
        $elements = [];
        foreach ($this->form->getElements() as $key=>$element) {

            if ($element instanceof \Vespula\Form\Element\Label) {
                $column = substr($key, 6);
                
                $labels[$column] = $element;
                $elements[$column] = $this->form->getElement($column);
            }
        }

        if ($action == self::ACTION_ADD) {
            $cancel_href = $this->router->urlFor($this->route_names['browse']);
        }

        if ($action == self::ACTION_EDIT) {
            $cancel_href = $this->router->urlFor($this->route_names['read'], ['id'=>$data[$record->getPrimaryCol()]]);
        }

        
        $content = $this->view->render($this->form_template, [
            'messages'=>$this->messages,
            'form'=>$this->form,
            'labels'=>$labels,
            'elements'=>$elements,
            'cancel_href'=>$cancel_href,
            'action'=>$action,
            'page_title'=>$this->getPageTitle($this->getClassShortName($this->mapper_class), $action)
        ]);

        return $content;
    }

    /**
     * Override this to modify the form builder etc. This is called before the form builder "builds"
     * 
     * @return void
     */
    protected function beforeBuildForm() :void
    {
    }

    /**
     * Sanitize the posted data. Override as necessary. This only works on strings
     * 
     * @return array Array of cleaned elements from post
     */
    protected function sanitizePost()  
    {
        $fields = array_keys($this->request->getParsedBody());
        $clean = [];


        foreach ($fields as $field) {
            $clean[$field] = filter_input(INPUT_POST, $field, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
        }

        return $clean;
    }

    protected function modifySelect(\Atlas\Mapper\MapperSelect $select)
    {
        return $select;
    }

    
    /**
     * Get the table name based on the mapper class
     * 
     * @param string $mapper_class
     * 
     * @return string The table name
     */
    protected function getTableName($mapper_class)
    {
        $table = $this->atlas->mapper($mapper_class)->getTable();
        return $table::NAME;
    }

    /** 
     * Get a default page title based on the table name
     * 
     * @param string $table_name
     * @param string $action
     * 
     * @return string
     */
    protected function getPageTitle($table_name, $action)
    {
        $record_name = str_replace(['-', '_'], ' ', ucwords($table_name, '-_'));
        if ($action == self::ACTION_EDIT) {
            $page_title = 'Edit ' .  $record_name . ' record';
        } else {
            $page_title = 'Add ' .  $record_name . ' record';
        }

        return $page_title;
    }

    
    /**
     * Get the shortened (non-namespaced) name of a class
     * 
     * @param string $class_name
     * 
     * @return string 
     */
    protected function getClassShortName($class_name)
    {
        $pos = strrpos($class_name, '\\');
        return substr($class_name, $pos + 1);
    }

    /**
     * Get the controllers route names (browse, read, edit, add, delete)
     * 
     * @return void
     */
    protected function setDefaultRouteNames()
    {
        $mapper_class = strtolower($this->getClassShortName($this->mapper_class));
        $route_names = [
            'browse'=>$mapper_class . '-browse',
            'read'=>$mapper_class . '-read',
            'edit'=>$mapper_class . '-edit',
            'add'=>$mapper_class . '-add',
            'delete'=>$mapper_class . '-delete',
        ];
        $this->route_names = $route_names;
    }

    /**
     * Set the route names by keys
     * 
     * @param array $route_names The route names
     */
    protected function setRouteNames(array $route_names)
    {
        $this->route_names = $route_names;
    }

    /**
     * Add a route name by key
     * 
     * @param array $route_names
     */
    protected function addRouteNames(array $route_names)
    {
        $this->route_names = array_merge($this->route_names, $route_names);
    }

    /**
     * Register events for the EventHandler Trait
     */
    protected function registerEvents()
    {
        // Override
        $this->on('update', function ($record) {
            $this->log->info('{record} Event: update - ID: {id} - User: {userid}', [
                'id'=>$record->id,
                'userid'=>$this->user->getUsername(),
                'record'=>$this->getClassShortName($this->mapper_class)
            ]);

            $this->setFlash('info', 'Record updated');
        });

        $this->on('update-error', function ($record, $message) {
            $this->log->error('{record} Event: update-error - ID: {id} - User: {userid} - Message: {message}', [
                'id'=>$record->id,
                'userid'=>$this->user->getUsername(),
                'record'=>$this->getClassShortName($this->mapper_class),
                'message'=>$message
            ]);

            $this->addErrorMessage('Update error: ' . $message);
        });

        $this->on('insert', function ($record) {
            $this->log->info('{record} Event: insert - ID: {id} - User: {userid}', [
                'id'=>$record->id,
                'userid'=>$this->user->getUsername(),
                'record'=>$this->getClassShortName($this->mapper_class)
            ]);

            $this->setFlash('info', 'Record inserted');
        });

        $this->on('insert-error', function ($record, $message) {
            $this->log->error('{record} Event: insert-error - User: {userid} - Message: {message}', [
                'userid'=>$this->user->getUsername(),
                'record'=>$this->getClassShortName($this->mapper_class),
                'message'=>$message
            ]);

            $this->addErrorMessage('Insert error: ' . $message);
        });

        $this->on('delete-error', function ($record, $message) {
            $this->setFlash('error', 'Failed to delete this item. ' . $message);
            $this->log->error('{record} Event: delete-error - ID: {id} - User: {userid} - Message: {message}' ,[
                'record'=>$this->getClassShortName($this->mapper_class),
                'id'=>$record->getPrimaryVal(),
                'userid'=>$this->user->getUsername(),
                'message'=>$message
            ]);
        });

        $this->on('delete', function ($record) {
            $this->log->info('{record} Event: delete - ID: {id} - User: {userid}', [
                'id'=>$record->id,
                'userid'=>$this->user->getUsername(),
                'record'=>$this->getClassShortName($this->mapper_class)
            ]);

            $this->setFlash('info', 'Record deleted');
        });
    }
}