<?php

namespace _Project_\Controllers;

use _Mapper_;

class _Class_ extends _Extends_
{
    protected $mapper_class;

    public function index()
    {
        $content = $this->view->render('_folder_/index', [
            'messages'=>$this->messages
        ]);
        $this->response->getBody()->write($content);
        return $this->response;
    }
}
