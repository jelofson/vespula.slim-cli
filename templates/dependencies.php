<?php

use Vespula\PlatesExtensions\Alerts;
use Vespula\Log\Log as Log;
use Vespula\Log\Adapter\ErrorLog;
use Vespula\Auth\Session\Session as AuthSession;
use Vespula\Auth\Auth;
use Vespula\Auth\Adapter\Text as AuthTextAdapter;
use Aura\Session\SessionFactory;
use League\Plates\Engine as View;
use Vespula\Locale\Locale;
use Vespula\User\User;
use Vespula\Form\Form;
use Vespula\Form\Builder\Builder as FormBuilder;
use Zend\Permissions\Acl\Acl;
use Vespula\Paginator\Decorator\Generic as PaginatorDecoratorGeneric;
use Vespula\Paginator\Paginator;
use Atlas\Orm\Atlas;

/**
 * @var \League\Container\Container $container
 */

$container->defaultToShared();

$container->add('settings', function () use ($settings) {
    return $settings;
});

$container->add('logAdapter', ErrorLog::class)->addArgument(ErrorLog::TYPE_PHP_LOG);
$container->add('log', Log::class)->addArgument('logAdapter');

$container->add('session', function () {
    $sessionFactory = new SessionFactory();
    return $sessionFactory->newInstance($_COOKIE);
});

$container->add('view', function () use ($container) {

    $settings = $container->get('settings');
    $theme = $settings['theme'];
    $title = $settings['title'];
    $base_uri = $settings['base_uri'];
    $env = $settings['env'];

    $views = dirname(__FILE__) . '/Views';
    $layouts = dirname(__FILE__) . '/Layouts';

    $view = new View($views);
    $view->addFolder('layouts', $layouts);

    $view->addData([
        'theme'=>$theme,
        'title'=>$title,
        'base_uri'=>$base_uri,
        'auth'=>$container->get('auth'),
        'router'=>$container->get('router'),
        'env'=>$env,
        'acl'=>$container->get('acl'),
        'user'=>$container->get('user'),
    ]);

    $alerts = new Alerts($container);
    $view->loadExtension($alerts);

    return $view;
});

$container->add('locale', Locale::class)->addArgument('en_CA')->addMethodCall('load', [__DIR__ . '/Locales']);
$container->add('user', User::class)->addArgument('guest');
$container->add('authSession', AuthSession::class);
$container->add('authAdapter', function () {
    $passwords = [
        'juser'=>'$2y$10$smIIzBtPIDz82ye/lTa5ruIqNIT7qFB3dbQr8ad9Xx9IRn9IF.l.C' // password is 'test'
    ];
    $adapter = new AuthTextAdapter($passwords);

    $adapter->setUserdata('juser', [
        'fullname'=>'Joe User',
        'email'=>'juser@example.com'
    ]);
    return $adapter;
});
$container->add('auth', Auth::class)->addArgument('authAdapter')->addArgument('authSession');
$pdo_settings = $settings['atlas']['pdo'];
$container->add('pdo', \PDO::class)->addArguments([
    $pdo_settings[0],
    $pdo_settings[1],
    $pdo_settings[2],
]);

$container->add('formBuilder', FormBuilder::class)
    ->addArgument('pdo')
    ->addMethodCall('setDefaultClasses', [
        $settings['form']['default_classes']
    ])
    ->addMethodCall('setBlacklist', [
        $settings['form']['blacklist']
    ]
);

$container->add('form', Form::class)
    ->addMethodCall('setBuilder', ['formBuilder'])
    ->addMethodCall('autoLf');

$container->add('acl', function () use ($container) {
    $acl = new Acl();
    // See the separate acl.php file for additional rules and resources.

    return $acl;
});

$container->add('paginatorDecorator', PaginatorDecoratorGeneric::class);
$container->add('paginator', Paginator::class)->addArgument('paginatorDecorator');


$container->add('atlas', function () use ($container) {
    $settings = $container->get('settings');
    $atlas = Atlas::new(
        $settings['atlas']['pdo'][0],
        $settings['atlas']['pdo'][1],
        $settings['atlas']['pdo'][2]
    );
    return $atlas;
});

