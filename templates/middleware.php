<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response;

// Note that last middleware added is first to be executed

$sampleMiddleWare = function (Request $request, RequestHandler $handler) {
    $response = $handler->handle($request);
    //$response->getBody()->write('This text is from middleware');
    return $response;
};


$app->add($sampleMiddleWare);

