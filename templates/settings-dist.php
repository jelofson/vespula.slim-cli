<?php

return [
    'env'=>'development',
    'displayErrorDetails'=>true,
    'logErrors'=>true,
    'logErrorDetails'=>true,
    'title'=>'Your Project',
    'theme'=>'default',
    'base_uri'=>'',
    'atlas'=>[
        'pdo'=>[
            'mysql:dbname=yourdb;host=localhost;charset=utf8',
            'juser',
            '********'
        ],
        'namespace'=>'_Project_\\Models',
        'directory'=>'src/_Project_/Models',
        'templates' => 'src/_Project_/Models/AtlasTemplates'
    ],
    'form'=>[
        'default_classes'=>[
            'text'=>'form-control',
            'textarea'=>'form-control',
            'select'=>'form-control',
        ],
        'blacklist'=>[
            'id'
        ]
    ],
    'roles'=>[
        'admin'=>[
            'juser'
        ]
    ]

];