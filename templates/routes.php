<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use _Project_\Controllers\Index;


$app->get('/', function (Request $request, Response $response, $args) {
    $controller = new Index($this, $request, $response);
    return $controller->index();
});
