<?php
declare(strict_types=1);

namespace {NAMESPACE}\{TYPE};

use _Project_\Models\Record;

/**
 * @method {TYPE}Row getRow()
 */
class {TYPE}Record extends Record
{
    use {TYPE}Fields;
}
