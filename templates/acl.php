<?php

$acl = $container->get('acl');
$acl->addResource('index');

$acl->addRole('guest');
$acl->addRole('user', 'guest');
$acl->addRole('admin', 'user');

$acl->allow('admin');

// Modify these up as needed.
$acl->allow('guest', null, 'browse');
$acl->allow('guest', null, 'read');


// Add resources and rules here.
