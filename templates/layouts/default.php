<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$this->e($title); ?></title>
    <link rel="stylesheet" type="text/css" href="<?=$base_uri; ?>/app/default/css/app.css" media="screen" />
</head>

<body>
    <h1><?=$this->e($title); ?></h1>
    <?=$this->section('content')?>

</body>

</html>