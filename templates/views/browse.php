<?php $this->layout('layouts::' . $theme); ?>

<?=$this->alerts($messages); ?>

<h2>Browse <?=$record_type; ?> Records</h2>

<p>[<a href="<?=$router->urlFor($add_route_name); ?>">Add new</a>]</p>

<div style="margin-bottom: 20px;">
<?php foreach ($records as $record) : ?>
    <table class="table table-bordered">
        <?php foreach ($columns as $col) : ?>
        <tr>
            <th><?=$col; ?></th>
            <td><?=$record->$col; ?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <th></th>
            <td><a href="<?=$router->urlFor($read_route_name, [$record->getPrimaryCol()=>$record->getPrimaryVal()]); ?>">View Full Record</a></td>
        </tr>
    </table>
    <hr style="border: 2px dashed black"/>
<?php endforeach; ?>
</div>
<?=$pagination; ?>