<?php $this->layout('layouts::' . $theme, ['title'=>$title]); ?>

<?=$this->alerts($messages); ?>

<h2>Delete Record</h2>

<h3>Are you sure you want to delete this <?=$record_type; ?> record?</h3>

<?php if ($related) : ?>
<p class="text-danger">You cannot delete this record directly as it has related records that could potentially be orphaned.</p>

<ul>
<?php foreach ($related as $name=>$count) : ?>
    <li><?=$name; ?> has <?=$count; ?> records.
<?php endforeach; ?>
</ul>
<p>You will have to manually delete those records first. Sorry.</p>

<a href="<?=$cancel_href; ?>" class="btn btn-default">Back</a>

<?php else : ?>
<?=$form->begin(); ?>
<?=$form->getElement('csrf_token'); ?>

<?=$form->submit('Delete')->class('btn btn-danger'); ?>
<a href="<?=$cancel_href; ?>" class="btn btn-default">Cancel</a>
<?=$form->end(); ?>
<?php endif; ?>


