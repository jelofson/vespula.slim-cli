<?php $this->layout('layouts::' . $theme, ['title'=>$title]); ?>

<h2><?=$page_title; ?></h2>

<?=$this->alerts($messages); ?>

<?=$form->begin(); ?>
<?=$form->getElement('csrf'); ?>

<?php foreach ($labels as $column=>$labelElement) : ?>
<div class="form-group">
    <?=$labelElement; ?>
    <?=$elements[$column]; ?>
</div>
<?php endforeach; ?>
<div>
    <?=$form->submit('Save')->class('btn btn-primary'); ?>
    <a href="<?=$cancel_href; ?>" class="btn btn-default">Cancel</a>
</div>
<?=$form->end(); ?>
