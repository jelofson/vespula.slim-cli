<?php $this->layout('layouts::' . $theme, ['title'=>$title]); ?>

<?=$this->alerts($messages); ?>

<h2><?=$this->e($message); ?></h2>

<?php if (isset($details)) : ?>
<p>
<?php echo $this->e($details); ?>
</p>
<?php endif; ?>