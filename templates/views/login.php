<?php $this->layout('layouts::' . $theme, ['title'=>$title]); ?>

<h1>Login</h1>

<?=$this->alerts($messages); ?>

<?php if (! $auth->isValid()) : ?>
<?=$form->begin(); ?>
<div class="form-group">
    <?=$form->label('Username')->for('username'); ?>
    <?=$form->text()->name('username')->id('username')->class('form-control'); ?>
</div>
<div class="form-group">
    <?=$form->label('Password')->for('password'); ?>
    <?=$form->password()->name('password')->id('password')->class('form-control'); ?>
</div>
<?=$form->button('Login')->type('submit')->class('btn btn-default'); ?>
<?=$form->end(); ?>


<?php else : ?>
<p>You are already logged in.</p>
<?php endif; ?>