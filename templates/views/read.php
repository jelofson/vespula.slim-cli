<?php $this->layout('layouts::' . $theme, ['title'=>$title]); ?>

<?=$this->alerts($messages); ?>

<h2><?=$record_type; ?> Record</h2>

<p>[<a href="<?=$router->urlFor($browse_route_name); ?>">Back to browse</a>]</p>

<table class="table table-bordered">
    <?php foreach ($columns as $col) : ?>
    <tr>
        <th><?=$col; ?></th>
        <td><?=$record->$col; ?></td>
    </tr>
    <?php endforeach; ?>
    <tr>
        <th></th>
        <td>
            [<a href="<?=$router->urlFor($edit_route_name, [$record->getPrimaryCol()=>$record->getPrimaryVal()]); ?>">Edit</a>]
            [<a href="<?=$router->urlFor($delete_route_name, [$record->getPrimaryCol()=>$record->getPrimaryVal()]); ?>">Delete</a>]
        </td>
    </tr>
</table>