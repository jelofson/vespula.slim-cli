<?php 
declare(strict_types = 1);

namespace Vespula\Cli;

class Project
{
    protected $name;
    protected $src_path;
    protected $cli_path;

    protected $log;

    public function __construct(string $cli_path, string $name, string $src_path, Log $log)
    {
        $this->cli_path = $cli_path;
        $this->src_path = $src_path;
        $this->name = $name;
        $log->reset();
        $this->log = $log;
        
        set_error_handler(function ($errnum, $errstr) {
            $this->log->error($errstr);
        });

        $this->log->debug('Building project named ' . $name);
        $this->log->debug('The CLI path is ' . $cli_path);
        $this->log->debug('The source path for the project is ' . $src_path);
        $this->log->debug('The path for the project is ' . $src_path . '/' . $name);
    }

    

    public function createFolders()
    {
        $this->log->debug('Creating folders...');
        if ($this->createProjectFolder()) {
            $this->log->info('Project folder created');
        }
        if ($this->createControllerFolder()) {
            $this->log->info('Controller folder created');
        }
        if ($this->createViewFolder()) {
            $this->log->info('Views folder created');
        }
        if ($this->createCommonViewsFolder()) {
            $this->log->info('Common views folder created');
        }
        if ($this->createModelFolder()) {
            $this->log->info('Models folder created');
        }
        if ($this->createAtlasTemplatesFolder()) {
            $this->log->info('Models Atlas folder created');
        }
        if ($this->createLayoutFolder()) {
            $this->log->info('Layouts folder created');
        }
        if ($this->createLayoutDefaultFolder()) {
            $this->log->info('Layouts default + assets folder created');
        }
        if ($this->createLocaleFolder()) {
            $this->log->info('Locales folder created');
        }
    }

    public function copyFiles()
    {
        $this->log->debug('Copying files...');
        if ($this->copyDependenciesFile()) {
            $this->log->info('Dependencies file copied');
        }
        if ($this->copyRoutesFile()) {
            $this->log->info('Routes file copied');
        }
        if ($this->copyMiddlewareFile()) {
            $this->log->info('Middleware file copied');
        }
        if ($this->copyAclFile()) {
            $this->log->info('Acl file copied');
        }
        if ($this->copySettingsDistFile()) {
            $this->log->info('Settings dist file copied');
        }
        if ($this->copySettingsFile()) {
            $this->log->info('Settings file copied');
        }
        if ($this->copyBaseControllerFile()) {
            $this->log->info('Base abstract controller file copied');
        }
        if ($this->copyBreadControllerFile()) {
            $this->log->info('Bread abstract controller file copied');
        }
        if ($this->copyBaseRecordFile()) {
            $this->log->info('Base record file copied');
        }
        if ($this->copyAtlasRecordTemplateFile()) {
            $this->log->info('Atlas Record template file copied');
        }
        if ($this->copyDefaultLayoutFile()) {
            $this->log->info('Default layout file copied');
        }
        if ($this->copyDefaultCssFile()) {
            $this->log->info('Default layout file copied');
        }
    }

    public function copyCommonViews()
    {
        if ($this->copyCommonFile('form')) {
            $this->log->info('Common form view file copied');
        }
        if ($this->copyCommonFile('delete')) {
            $this->log->info('Common delete view file copied');
        }
        if ($this->copyCommonFile('read')) {
            $this->log->info('Common read view file copied');
        }
        if ($this->copyCommonFile('browse')) {
            $this->log->info('Common browse view file copied');
        }
        if ($this->copyCommonFile('login')) {
            $this->log->info('Common login view file copied');
        }
        if ($this->copyCommonFile('error')) {
            $this->log->info('Common error view file copied');
        }
    }

    public function createAppSymlink()
    {
        $html_folder = dirname($this->src_path) . '/html';
        if (! file_exists($html_folder . '/app')) {
            $this->log->debug('Making app subfolder in html');
            mkdir($html_folder . '/app');
        }

        $link = $html_folder . '/app/default';
        $target = $this->src_path . '/' . $this->name . '/Layouts/default/assets';
        
        if (! file_exists($link)) {
            $this->log->debug('Symlinking the default assets in the html/app folder');
            return symlink($target, $link);
        }

        $this->log->debug('Symlink already exists in the html/app folder');
        return false;
    }

    protected function createProjectFolder() :bool
    {
        if (! file_exists($this->src_path . '/' . $this->name)) {
            return mkdir($this->src_path . '/' . $this->name);
        }

        $this->log->info("Project folder $this->name exists so not creating");
        return false;
    }

    protected function createControllerFolder() :bool
    {

        if (! file_exists($this->src_path . '/' . $this->name . '/Controllers')) {
            return mkdir($this->src_path . '/' . $this->name . '/Controllers');
        }

        $this->log->info("Controllers folder exists so not creating");
        return false;
    }

    protected function createViewFolder() :bool
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Views')) {
            return mkdir($this->src_path . '/' . $this->name . '/Views');
        }

        $this->log->info("Views folder exists so not creating");
        return false;
    }

    protected function createCommonViewsFolder() :bool
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Views/common')) {
            return mkdir($this->src_path . '/' . $this->name . '/Views/common');
        }

        $this->log->info("Common views folder exists so not creating");
        return false;
    }

    protected function createModelFolder() :bool
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Models')) {
            return mkdir($this->src_path . '/' . $this->name . '/Models');
        }

        $this->log->info("Models folder exists so not creating");
        return false;
    }

    protected function createAtlasTemplatesFolder() :bool
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Models/AtlasTemplates')) {
            return mkdir($this->src_path . '/' . $this->name . '/Models/AtlasTemplates');
        }

        $this->log->info("Models folder exists so not creating");
        return false;
    }

    protected function createLayoutFolder() :bool
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Layouts')) {
            return mkdir($this->src_path . '/' . $this->name . '/Layouts');
        }

        $this->log->info("Layouts folder exists so not creating");
        return false;
    }

    protected function createLayoutDefaultFolder() :bool
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Layouts/default')) {
            return mkdir($this->src_path . '/' . $this->name . '/Layouts/default/assets/css', 0775, true);
        }

        $this->log->info("Layouts default subfolder exists so not creating");
        return false;
    }

    protected function createLocaleFolder() :bool
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Locales')) {
            return mkdir($this->src_path . '/' . $this->name . '/Locales');
        }

        $this->log->info("Locales folder exists so not creating");
        return false;
    }

    protected function copyDependenciesFile()
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/dependencies.php')) {
            if (file_exists($this->cli_path . '/templates/dependencies.php')) {
                return copy($this->cli_path . '/templates/dependencies.php', $this->src_path . '/' . $this->name . '/dependencies.php');
            }
            $this->log->error('Could not locale the dependencies template!');
            return false;
            
        }

        $this->log->info("Dependencies file exists so not creating");
        return false;
    }

    protected function copyRoutesFile()
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/routes.php')) {
            if (file_exists($this->cli_path . '/templates/routes.php')) {
                $routes = file_get_contents($this->cli_path . '/templates/routes.php');
                $routes = str_replace('_Project_', $this->name, $routes);
                return file_put_contents($this->src_path . '/' . $this->name . '/routes.php', $routes);
            }
            $this->log->error('Could not locale the routes template!');
            return false;
            
        }

        $this->log->info("Routes file exists so not creating");
        return false;
    }

    protected function copyMiddlewareFile()
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/middleware.php')) {
            if (file_exists($this->cli_path . '/templates/middleware.php')) {
                return copy($this->cli_path . '/templates/middleware.php', $this->src_path . '/' . $this->name . '/middleware.php');
            }
            $this->log->error('Could not locale the middleware template!');
            return false;
            
        }

        $this->log->info("Middleware file exists so not creating");
        return false;
    }

    protected function copyAclFile()
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/acl.php')) {
            if (file_exists($this->cli_path . '/templates/acl.php')) {
                return copy($this->cli_path . '/templates/acl.php', $this->src_path . '/' . $this->name . '/acl.php');
            }
            $this->log->error('Could not locate the acl template!');
            return false;
            
        }

        $this->log->info("Middleware file exists so not creating");
        return false;
    }

    protected function copySettingsDistFile()
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/settings-dist.php')) {
            if (file_exists($this->cli_path . '/templates/settings-dist.php')) {
                $settings = file_get_contents($this->cli_path . '/templates/settings-dist.php');
                $settings = str_replace('_Project_', $this->name, $settings);
                return file_put_contents($this->src_path . '/' . $this->name . '/settings-dist.php', $settings);
                
            }
            $this->log->error('Could not locate the settings-dist template!');
            return false;
            
        }

        $this->log->info("Settings-dist file exists so not creating");
        return false;
    }

    protected function copySettingsFile()
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/settings.php')) {
            if (file_exists($this->cli_path . '/templates/settings-dist.php')) {
                $settings = file_get_contents($this->cli_path . '/templates/settings-dist.php');
                $settings = str_replace('_Project_', $this->name, $settings);
                return file_put_contents($this->src_path . '/' . $this->name . '/settings.php', $settings);
            }
            $this->log->error('Could not locate the settings-dist template!');
            return false;
            
        }

        $this->log->info("Settings file exists so not creating");
        return false;
    }

    protected function copyBaseControllerFile()
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Controllers/Controller.php')) {
            if (file_exists($this->cli_path . '/templates/base.php')) {
                $base = file_get_contents($this->cli_path . '/templates/base.php');
                $base = str_replace('_Project_', $this->name, $base);
                return file_put_contents($this->src_path . '/' . $this->name . '/Controllers/Controller.php', $base);
            }
            $this->log->error('Could not locate the base controller template!');
            return false;
            
        }

        $this->log->info("Base controller file exists so not creating");
        return false;
    }

    protected function copyBreadControllerFile()
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Controllers/Bread.php')) {
            if (file_exists($this->cli_path . '/templates/bread.php')) {
                $bread = file_get_contents($this->cli_path . '/templates/bread.php');
                $bread = str_replace('_Project_', $this->name, $bread);
                return file_put_contents($this->src_path . '/' . $this->name . '/Controllers/Bread.php', $bread);
            }
            $this->log->error('Could not locate the bread controller template!');
            return false;
            
        }

        $this->log->info("Bread controller file exists so not creating");
        return false;
    }

    public function copyBaseRecordFile()
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Models/Record.php')) {
            if (file_exists($this->cli_path . '/templates/record.php')) {
                $record = file_get_contents($this->cli_path . '/templates/record.php');
                $record = str_replace('_Project_', $this->name, $record);
                return file_put_contents($this->src_path . '/' . $this->name . '/Models/Record.php', $record);
            }
            $this->log->error('Could not locate the record template!');
            return false;
        }

        $this->log->info("A base record already exists so I will not overwrite");
        return false;
    }

    public function copyAtlasRecordTemplateFile()
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Models/AtlasTemplates/TypeRecord.tpl')) {
            if (file_exists($this->cli_path . '/templates/atlas/TypeRecord.tpl')) {
                $template = file_get_contents($this->cli_path . '/templates/atlas/TypeRecord.tpl');
                $template = str_replace('_Project_', $this->name, $template);
                return file_put_contents($this->src_path . '/' . $this->name . '/Models/AtlasTemplates/TypeRecord.tpl', $template);
            }
            $this->log->error('Could not locate the Atlas TypeRecord template!');
            return false;
        }

        $this->log->info("An Atlas Record Template already exists so I will not overwrite");
        return false;
    }

    protected function copyDefaultLayoutFile()
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Layouts/default.php')) {
            if (file_exists($this->cli_path . '/templates/layouts/default.php')) {
                return copy($this->cli_path . '/templates/layouts/default.php', $this->src_path . '/' . $this->name . '/Layouts/default.php');
            }
            $this->log->error('Could not locate the default layout template!');
            return false;
            
        }

        $this->log->info("Settings file exists so not creating");
        return false;
    }

    protected function copyDefaultCssFile()
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Layouts/default/assets/css/app.css')) {
            if (file_exists($this->cli_path . '/templates/layouts/default/assets/css/app.css')) {
                return copy($this->cli_path . '/templates/layouts/default/assets/css/app.css', $this->src_path . '/' . $this->name . '/Layouts/default/assets/css/app.css');
            }
            $this->log->error('Could not locate the default css template!');
            return false;
            
        }

        $this->log->info("Default css file exists so not creating");
        return false;
    }

    

    protected function copyCommonFile($file)
    {
        if (! file_exists($this->src_path . '/' . $this->name . '/Views/common/' . $file . '.php')) {
            if (file_exists($this->cli_path . '/templates/views/' . $file . '.php')) {
                return copy($this->cli_path . '/templates/views/' . $file . '.php', $this->src_path . '/' . $this->name . '/Views/common/' . $file . '.php');
            }
            $this->log->error('Could not locate the common ' . $file . ' template!');
            return false;
        }

        $this->log->info("Common $file file exists so not creating");
        return false;
    }


    public function __destruct()
    {
        restore_error_handler();
    }
}
