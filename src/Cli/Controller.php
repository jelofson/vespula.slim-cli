<?php
declare(strict_types = 1);

namespace Vespula\Cli;

class Controller
{
    protected $name;
    protected $cli_path;
    protected $src_path;
    protected $extends;
    protected $log;
    protected $project_name;
    protected $mapper_class;

    public function __construct(string $name, string $cli_path, string $src_path, string $extends, string $mapper_class, Log $log)
    {
        $this->name = $name;    
        $this->cli_path = $cli_path;
        $this->src_path = $src_path;
        $this->extends = $extends;
        $log->reset();    
        $this->log = $log; 
        
        $this->project_name = $this->getProjectName();
        $this->mapper_class = $mapper_class;
        
        
        set_error_handler(function ($errnum, $errstr) {
            $this->log->error($errstr);
        });

        $this->log->debug('Building controller named ' . $name);
        $this->log->debug('The cli path is ' . $cli_path);
        $this->log->debug('The path for the project is ' . $src_path);
        $this->log->debug('The project name is '. $this->project_name);
        $this->log->debug('The controller will extend ' . $extends);
        if ($mapper_class) {
            $this->log->debug('The controller will use a mapper class called ' . $mapper_class);
        } else {
            $this->log->debug('A mapper class was not specified');
        }

    }

    protected function getProjectName()
    {
        $pos = strrpos($this->src_path, '/');
        return substr($this->src_path, $pos + 1);
    }

    protected function getShortClassName($class)
    {
        $pos = strrpos($class, '\\');
        return substr($class, $pos + 1);
    }

    public function copyFiles()
    {

        if ($this->copyControllerFile()) {
            $this->log->info('Copied the controller file to the Controllers folder');
        }

        if ($this->createViewSubFolder()) {
            $this->log->info('Created the view subfolder in the Views folder');
        }

        if ($this->copyIndexView()) {
            $this->log->info('Copied the index view file to the Views folder');
        }
    }

    protected function copyControllerFile()
    {
        if (file_exists($this->cli_path . '/templates/controller.php')) {
            $controller = file_get_contents($this->cli_path . '/templates/controller.php');
            $controller = str_replace('_Project_', $this->project_name, $controller);
            $controller = str_replace('_Class_', $this->name, $controller);
            $controller = str_replace('_Extends_', $this->extends, $controller);
            $controller = str_replace('_folder_', strtolower($this->name), $controller);
            if ($this->mapper_class) {
                $controller = str_replace('_Mapper_', $this->mapper_class, $controller);
                $mapper_shortname = $this->getShortClassName($this->mapper_class);
                $controller = str_replace('protected $mapper_class;', 'protected $mapper_class = ' . $mapper_shortname . '::class;', $controller);
            } else {
                $controller = str_replace('use _Mapper_;', '', $controller);
            }
            
            return file_put_contents($this->src_path . '/Controllers/' . $this->name . '.php', $controller);
        }
        $this->log->error('Could not locate the controller template!');
        return false;
    }

    protected function createViewSubFolder()
    {
        if (! file_exists($this->src_path . '/Views/' . strtolower($this->name))) {
            return mkdir($this->src_path . '/Views/' . strtolower($this->name));
        }

        $this->log->info("Controller view subfolder exists so not creating");
        return false;
    }

    protected function copyIndexView()
    {
        if (! file_exists($this->src_path . '/Views/' . strtolower($this->name) . '/index.php')) {
            if (file_exists($this->cli_path . '/templates/views/index.php')) {
                return copy($this->cli_path . '/templates/views/index.php', $this->src_path . '/Views/' . strtolower($this->name) . '/index.php');
            }
            $this->log->error('Could not locale the index view template!');
            return false;
            
        }

        $this->log->info("index view file exists so not creating");
        return false;
    }

    public function addBreadRoutes()
    {
        $this->log->info('Adding bread routes to end of routes file');
        if (! $this->mapper_class) {
            $this->error('No mapper class. Cannot add routes without a mapper class name');
            return;
        }

        $this->log->debug('Routes are for \\' . $this->project_name . '\\Controllers\\' . $this->name);
        // Make sure there is a routes file
        $routes_file = $this->src_path . '/routes.php';
        $route_template = $this->cli_path . '/templates/route.txt';
        if (file_exists($routes_file) && file_exists($route_template)) {
            $template = file_get_contents($route_template);
            $mapper = strtolower($this->getShortClassName($this->mapper_class));
            $mapper = str_ireplace('::class', '', $mapper);
            $controller_lower = strtolower($this->name);

            $route_actions = [
                'browse'=>[
                    'httpmethod'=>'get',
                    'href'=>'/' . $controller_lower . '',
                    'params'=>'',
                    'name'=>$mapper . '-browse'
                ],
                'read'=>[
                    'httpmethod'=>'get',
                    'href'=>'/' . $controller_lower . '/read/{id}',
                    'params'=>'$args[\'id\']',
                    'name'=>$mapper . '-read'
                ],
                'edit'=>[
                    'httpmethod'=>'get',
                    'href'=>'/' . $controller_lower . '/edit/{id}',
                    'params'=>'$args[\'id\']',
                    'name'=>$mapper . '-edit'
                ],
                'doEdit'=>[
                    'httpmethod'=>'post',
                    'href'=>'/' . $controller_lower . '/edit/{id}',
                    'params'=>'$args[\'id\']',
                    'name'=>null
                ],
                'add'=>[
                    'httpmethod'=>'get',
                    'href'=>'/' . $controller_lower . '/add',
                    'params'=>'',
                    'name'=>$mapper . '-add'
                ],
                'doAdd'=>[
                    'httpmethod'=>'post',
                    'href'=>'/' . $controller_lower . '/add',
                    'params'=>'',
                    'name'=>null
                ],
                'delete'=>[
                    'httpmethod'=>'get',
                    'href'=>'/' . $controller_lower . '/delete/{id}',
                    'params'=>'$args[\'id\']',
                    'name'=>$mapper . '-delete'
                ],
                'doDelete'=>[
                    'httpmethod'=>'post',
                    'href'=>'/' . $controller_lower . '/delete/{id}',
                    'params'=>'$args[\'id\']',
                    'name'=>null
                ],
            ];


            $routes = [];
            
            foreach ($route_actions as $method=>$details) {
                $route = str_replace('_httpmethod_', $details['httpmethod'], $template);
                $route = str_replace('_href_', $details['href'], $route);
                $route = str_replace('_controller_', '\\' . $this->project_name . '\\Controllers\\' . $this->name, $route);
                $route = str_replace('_method_', $method, $route);
                $route = str_replace('_params_', $details['params'], $route);
                $name = '';
                if ($details['name']) {
                    $name = '->setName(\'' . $details['name'] . '\')';
                }
                $route = str_replace('_name_', $name, $route);
                $routes[] = $route;
            }
            
            $all_routes = PHP_EOL . '// Bread routes for ' . $this->project_name . '\\' . $this->name . PHP_EOL;
            $all_routes .= implode(PHP_EOL . PHP_EOL, $routes) . PHP_EOL . PHP_EOL;

            return file_put_contents($routes_file, $all_routes, FILE_APPEND);

        }

        $this->log->error('Either the routes file or routes template were missing so bread routes NOT added');
        return false;
    }

    public function addIndexRoute()
    {
        $this->log->info('Adding index route to end of routes file');
        $this->log->debug('Route is for \\' . $this->project_name . '\\Controllers\\' . $this->name);

        // Make sure there is a routes file
        $routes_file = $this->src_path . '/routes.php';
        $route_template = $this->cli_path . '/templates/route.txt';
        if (file_exists($routes_file) && file_exists($route_template)) {
            
            $template = file_get_contents($route_template);
            $controller_lower = strtolower($this->name);
            
            $route = str_replace('_httpmethod_', 'get', $template);
            $route = str_replace('_href_', '/' . $controller_lower, $route);
            $route = str_replace('_controller_', '\\' . $this->project_name . '\\Controllers\\' . $this->name, $route);
            $route = str_replace('_method_', 'index', $route);
            $route = str_replace('_params_', '', $route);

            $name = '->setName(\'' . $controller_lower . '-index' . '\')';
            
            $route = str_replace('_name_', $name, $route) . PHP_EOL . PHP_EOL;
            $comment = PHP_EOL . '// Index Route for ' . $this->project_name . '\\' . $this->name . PHP_EOL;

            return file_put_contents($routes_file, $comment . $route, FILE_APPEND);

        }

        $this->log->error('Either the routes file or routes template were missing so index route NOT added');
        return false;
    }

    public function appendAclRules($mapper_class)
    {
        $mapper = $this->getShortClassName($mapper_class);
        $resource = strtolower($mapper);

        $acl_file = $this->src_path . '/acl.php';

        if (file_exists($acl_file)) {
            $this->log->debug('ACL file exists...preparing to append acl resource');
            $acl = file_get_contents($acl_file);
            $action = '$acl->addResource(\'' . $resource . '\');';
            $this->log->info('Appending acl resource to acl file');
            return file_put_contents($acl_file, PHP_EOL . $action . PHP_EOL, FILE_APPEND);
        }

        $this->log->error('Acl file does not exist so not adding acl resource');
        return false;
    }

    public function addLoginLogoutLogic($controller_file = null)
    {
        if (! $controller_file) {
            $controller_file = $this->src_path . '/Controllers/' . $this->name . '.php';
        }
        
        if (file_exists($controller_file)) {
            $controller = file_get_contents($controller_file);
            $logic = file_get_contents($this->cli_path . '/templates/login_logout.txt');

            $last_brace_pos = strrpos($controller, '}');

            $current_text = substr($controller, 0, $last_brace_pos);

            $new_text = $current_text . PHP_EOL . PHP_EOL . $logic . PHP_EOL . '}' . PHP_EOL;
            $this->log->info('Adding login logic to ' . $controller_file);
            return file_put_contents($controller_file, $new_text);
        }

        $this->log->debug('Controller file does not exist so not appending login login');
        return false;
    }

    public function addLoginLogoutRoutes()
    {
        $this->log->info('Adding login logout route to end of routes file');
        $this->log->debug('Route is for \\' . $this->project_name . '\\Controllers\\' . $this->name);

        $this->addLoginGetRoute();
        $this->addLoginPostRoute();
        $this->addLogoutRoute();
        
    }

    protected function addLoginGetRoute()
    {
        // Make sure there is a routes file
        $routes_file = $this->src_path . '/routes.php';
        $route_template = $this->cli_path . '/templates/route.txt';
        if (file_exists($routes_file) && file_exists($route_template)) {
            
            $template = file_get_contents($route_template);
            $controller_lower = strtolower($this->name);
            
            $route = str_replace('_httpmethod_', 'get', $template);
            $route = str_replace('_href_', '/login', $route);
            $route = str_replace('_controller_', '\\' . $this->project_name . '\\Controllers\\' . $this->name, $route);
            $route = str_replace('_method_', 'login', $route);
            $route = str_replace('_params_', '', $route);

            $name = '->setName(\'' . 'login' . '\')';
            
            $route = str_replace('_name_', $name, $route) . PHP_EOL . PHP_EOL;
            $comment = PHP_EOL . '// Login Route for ' . $this->project_name . '\\' . $this->name . PHP_EOL;

            return file_put_contents($routes_file, $comment . $route, FILE_APPEND);

        }

        $this->log->error('Either the routes file or routes template were missing so get login route NOT added');
        return false;
    }

    protected function addLoginPostRoute()
    {
        // Make sure there is a routes file
        $routes_file = $this->src_path . '/routes.php';
        $route_template = $this->cli_path . '/templates/route.txt';
        if (file_exists($routes_file) && file_exists($route_template)) {
            
            $template = file_get_contents($route_template);
            $controller_lower = strtolower($this->name);
            
            $route = str_replace('_httpmethod_', 'post', $template);
            $route = str_replace('_href_', '/login', $route);
            $route = str_replace('_controller_', '\\' . $this->project_name . '\\Controllers\\' . $this->name, $route);
            $route = str_replace('_method_', 'doLogin', $route);
            $route = str_replace('_params_', '', $route);

            $name = '';
            
            $route = str_replace('_name_', $name, $route) . PHP_EOL . PHP_EOL;
            $comment = PHP_EOL . '// Login Route (POST) for ' . $this->project_name . '\\' . $this->name . PHP_EOL;

            return file_put_contents($routes_file, $comment . $route, FILE_APPEND);

        }

        $this->log->error('Either the routes file or routes template were missing so post login route NOT added');
        return false;
    }

    protected function addLogoutRoute()
    {
        // Make sure there is a routes file
        $routes_file = $this->src_path . '/routes.php';
        $route_template = $this->cli_path . '/templates/route.txt';
        if (file_exists($routes_file) && file_exists($route_template)) {
            
            $template = file_get_contents($route_template);
            $controller_lower = strtolower($this->name);
            
            $route = str_replace('_httpmethod_', 'get', $template);
            $route = str_replace('_href_', '/logout', $route);
            $route = str_replace('_controller_', '\\' . $this->project_name . '\\Controllers\\' . $this->name, $route);
            $route = str_replace('_method_', 'logout', $route);
            $route = str_replace('_params_', '', $route);

            $name = '->setName(\'' . 'logout' . '\')';
            
            $route = str_replace('_name_', $name, $route) . PHP_EOL . PHP_EOL;
            $comment = PHP_EOL . '// Logout Route for ' . $this->project_name . '\\' . $this->name . PHP_EOL;

            return file_put_contents($routes_file, $comment . $route, FILE_APPEND);

        }

        $this->log->error('Either the routes file or routes template were missing so logout route NOT added');
        return false;
    }


    public function __destruct()
    {
        restore_error_handler();
    }
}
