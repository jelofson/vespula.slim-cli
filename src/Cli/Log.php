<?php
declare(strict_types = 1);

namespace Vespula\Cli;

class Log
{
    protected $log = [];

    public function info(string $message)
    {
        $this->logEvent('info', $message);
    }

    public function debug(string $message)
    {
        $this->logEvent('debug', $message);
    }

    public function error(string $message)
    {
        $this->logEvent('error', $message);
    }

    public function getEntries($level = null)
    {
        if (! $level) {
            return $this->log;
        }

        $log = [];
        foreach ($this->log as $info) {
            if ($info['level'] == $level) {
                $log[] = $info;
            }
        }
        return $log;
    }

    public function toLines()
    {
        $lines = [];
        foreach ($this->log as $item) {
            switch($item['level']) {
                case 'info' :
                    $lines[] = "[INFO]\t{$item['timestamp']}\t{$item['message']}";
                break;
                case 'error' :
                    $lines[] = "[ERROR]\t{$item['timestamp']}\t{$item['message']}";
                break;
                case 'debug' : 
                    $lines[] = "[DEBUG]\t{$item['timestamp']}\t{$item['message']}";
                    
                break;
            }
        }

        return $lines;
    }

    public function logEvent($level, $message)
    {
        $now = new \DateTime();
        $data = [
            'level'=>$level,
            'timestamp'=>$now->format('H:i:s u'),
            'message'=>$message
        ];
        array_push($this->log, $data);
    }

    public function reset()
    {
        $this->log = [];
    }
}